
#ifndef GEOMETRY_H
#define GEOMETRY_H

// A line can be classified as having segments (including/excluding either endpoints) or as being infinite
typedef enum LineType {
    NO_SPECIAL_LINE,
    SEGMENT_CLOSED,
    SEGMENT_OPEN,
    SEGMENT_HALF_OPEN,
    INFINITE
} LineType;

// A triangle can be classified based on its Angles
typedef enum TriangleAngleType {
    NO_SPECIAL_ANGLE,
    ACUTE = 89, // Defining Acute to be '89' which is less than 90 only for comparison purposes.
    RIGHT, // 90, which is the degrees of a right triangle
    OBTUSE // 91... which is >90 which defines an obtuse triangle
} TriangleAngleType;

// A triangle can be classified based on its Sides
typedef enum TriangleSideType {
    SCALENE, // This is zero, no equal sides, which defines a Scalene triangle
    ISOSCELES = 2, // Defining Isoosceles to be '2' since that is the number of sides an isosceles triangle has.
    EQUILATERAL, // 3 which defines an equilateral triangle
} TriangleSideType;

// Every 2D shape has a lengthAround (perimeter or circumference) and an area
typedef struct ShapeProperties2D {
    float lengthAround;
    float area;
} ShapeProperties2D;

// A 2D point consists of two values, an x and a y coordinate
typedef struct Point2D {
    float x;
    float y;
} Point2D;

// A line has 2 points, a length, a slope, and a y intercept
typedef struct Line {
    Point2D p1;
    Point2D p2;
    float length;
    float slope;
    float yIntercept;
    LineType lineType;
} Line;

// A triangle has 3 lines and can be classified based on its angles and sides
typedef struct Triangle {
    Line l1;
    Line l2;
    Line l3;
    TriangleAngleType angleType;
    TriangleSideType sideType;
    ShapeProperties2D properties;
} Triangle;

// A square has 4 lines and 2D properties
typedef struct Square {
    Line l1;
    Line l2;
    Line l3;
    Line l4;
    ShapeProperties2D properties;
} Square;

// A rectangle has 4 lines and 2D properties
typedef struct Rectangle {
    Line l1;
    Line l2;
    Line l3;
    Line l4;
    ShapeProperties2D properties;
} Rectangle;

// A circle has a center point, a radius, and 2D properties
typedef struct Circle {
    Point2D center;
    float radius;
    ShapeProperties2D properties;
} Circle;


Line* CreateLinePoints(Point2D p1, Point2D p2, LineType lineType);
Line* CreateLinePointSlope(Point2D p, float slope, LineType lineType);




// UTILITY FUNCTIONS

float DoPythagoreanTheorem(float a, float b);






#endif // GEOMETRY_H











