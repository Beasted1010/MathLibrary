
#ifndef MY_MATH_H
#define MY_MATH_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

//CONSTANTS
static const float PI = 3.1415926535f;
static const float e = 2.718281828f;

typedef _Bool uint1_t;

float Exponentiate( float base, float exponent );
float DecimalExponentExponentiate( float base, float exponent );
// TODO: Currently causes issue with float SquareNumber( float base );
float PerToDec( float rateInPer );
float DecimalExponentExponetiate( float base, float exponent );
float SquareNumber( float base );
float AbsoluteValue( float val );
float SquareRoot( float val );
float NthRoot( int nth_root, float val );
uint1_t IsFactor( int val, int divisor );
void FindFactors( int val, int** out, int* numFactors );
uint1_t IsPrime( int val );
void DoPrimeFactorization( int val, int** out, int* numPrimeFactors );
int FindGCD( int a, int b );


#endif /* my_math.h */
