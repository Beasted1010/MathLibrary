
#include <my_math.h>
#include <common.h>

float Exponentiate( float base, float exponent )
{
    int i;
    float result = 1;
    for( i = 0; i < exponent; i++ )
    { result *= base; }

    return result;
}

float DecimalExponentExponentiate( float base, float exponent )
{
    float result = 1;
    // TODO: Need to figure out way to calculate an irrational exponent
    return result;
}

float SquareNumber( float base )
{
    return Exponentiate( base, 2 );
}

float PerToDec( float rateInPer )
{
    return rateInPer / 100;
}

// MISCELLANEOUS FUNCTIONS

float AbsoluteValue( float val )
{
    if( val >= 0 )
        return val;
    else
        return -val;
}

float SquareRoot( float val )
{
    NthRoot(2, val);
}

float NthRoot( int nth_root, float val )
{
    float threshold = 0.001;
    //printf("nth_root = %i\nthreshold = %f\n\n", threshold);

    int num_iterations = 0;

    // TODO: NEED TO CALCULATE THIS, PERHAPS WITH SOME SORT OF SQUEEZE LIMIT THINGY? estimate.
    float guess = val / 2.0;
    int converged = 0;
    float f_x;
    float f_prime_x;

    float old_guess = guess;

    while(!converged)
    {
        f_x = Exponentiate(guess, nth_root) - val; // Function x^n - i = 0, from x = nth_root(i)
        f_prime_x = nth_root * Exponentiate(guess, nth_root - 1); // Power rule -> n * (x^(n-1))
        //printf("f_x = %f\nf_prime_x = %f\n\n", f_x, f_prime_x);

        old_guess = guess;
        guess = guess - (f_x / f_prime_x); // Update guess
        //printf("old_guess = %f\nnew guess = %f\n\n", old_guess, guess);

        num_iterations++;

        //printf("Change = %f\n", AbsoluteValue(old_guess - guess));
        if( AbsoluteValue(old_guess - guess) < threshold)
            converged = 1;
    }

    //printf("%i root of %f = %f", nth_root, val, guess);
    printf("Calculation took %i iterations\n", num_iterations);

    return guess;
}



// UTILITY FUNCTIONS

uint1_t IsFactor( int val, int divisor )
{
    // If the value can not be evenly divided by the divisor (remainder != 0), then the divisor is a not factor
    if( val % divisor )
        return 0;

    // The divisor evenly divides the value, so it is a factor
    return 1;
}

void FindFactors( int val, int** out, int* numFactors )
{
    ValidateObject( *out, "findFactorsOut");
    *numFactors = 0;

    for( int i = 1; i <= val; i++ )
    {
        if( IsFactor(val, i) )
        {
            int multiplicand = i; // The left part of factor pair
            int multiplier = val / i; // The right part of factor pair

            // If the right side of the factor pair is greater then the left
            //      then that means we have already considered it (since we count up from 1),
            //      and thus we are at our stopping point since we don't want to count the same factors twice.
            if( multiplicand > multiplier )
                break;

            *out = realloc( *out, sizeof(int) * (*numFactors + 2) );
            
            (*out)[*numFactors] = multiplicand;
            *numFactors += 1;
            (*out)[ (*numFactors)++ ] = multiplier;
        }
    }

}

uint1_t IsPrime( int val )
{
    // 2 and 3 are prime
    if( val == 2 || val == 3 )
        return 1;

    // This covers the 0,2,3,4,6 numbers and all of their multiples. So there are only numbers of the form 6k+/-1
    //      left (i.e. 1 and 5)
    if( val % 2 == 0 || val % 3 == 0 )
        return 0;

    // Using the fact that every prime is of the form 6k +/- 1
    // This is because if a number leaves a remainder of 0, 2, or 4 when divided by 6
    //      then it is even, and thus not prime (unless 2).
    //      This is because the remainder is sort of saying "how far a number is" from another.
    //          So if a number is some multiple of 0, 2, or 4 away from 6, then it must be some even number.
    // If a number leaves a remainder of 3, then it is some multiple of 3 away from 6, thus divisible by 3.
    //      So it is not prime (unless it is 3 itself).
    // This leaves only 1 and 5, which can be represented as numbers of the form 6k +/- 1

    //      0   1   2   3   4   5   6       -> This is the value used for k in 6k +/- 1 (+/- indicated by LHS column)
    //      __________________________
    // +1 | 1   7   13  19  25  31  37
    // -1 | -1  5   11  17  23  29  35
    //
    // As seen in the above table (ignoring 1 and -1), 
    //      starting at 5 our value oscillates between adding 2 and adding 4 to the previous value to get next closest
    // We will use this observation for our approach at only checking numbers that are of this 6k +/- 1 form

    int currentResult = 5;
    int currentStep = 2;

    // Those values below 25 are all prime of this form
    //if( val > 1 && (currentResult * currentResult < val) )
    //    return 1;

    // We are employing the fact that we only need to test the numbers less than sqrt(val) to determine primeness
    // The fact that we only need to check up to the square root of a number to determine if it is prime comes from
    //      the observation that if n = a*b then a and b both can't be greater than the sqrt(n)
    //      or else the result would be larger than n. This shows that if a number is not prime, then it has at 
    //      least one factor that is somewhere between the sqrt(n) and 1 (not including 1)
    // Above we ensured that the value isn't one of the numbers > 1 of this 6k +/- 1 form, this is because all of
    //      the numbers below 25 (5*5) and > 1 are prime of this form. This ensures we don't exclude numbers < 25
    while( currentResult * currentResult <= val) 
    {
        // If there is a remainder after dividing val by the current 6k +/- 1 result then it is not prime
        if( IsFactor(val, currentResult) )
            return 0;
            
        // Update the current result to step up to the next value of the form 6k +/- 1
        currentResult += currentStep;

        // Update step to be 4 if it was 2, and 2 if it was 4. This serves to oscillate the step between 4 and 2.
        // The reason for this can be seen by observing that for all the column values in our table, 
        //      we always move 6 up from one column to the immediate right column for the same row.
        //      But we want to hit all of the values of this 6k +/- 1 form, so we won't be going up by 6.
        //      Instead the amount to increase will depend on which row we are currently in.
        //      Let's first observe that to go from the -1 row to the 1 row we remain in the same column,
        //          this is simply because we start from our smallest 6k+/-1 form (which is -1) and move up from there.
        //          Each next value we come across is the next smallest value of this form (i.e. in our table).
        //          While in the -1 row our next smallest is in the +1 row, but in the same column.
        //          This is to hit all the values of this 6k+/-1 form. So if we are in the -1 row, our next value will
        //              always be in the +1 row but under the same column.
        //          The same column part is important because this tells us that to make this step we must go from
        //              6k-1 to 6k+1 for the same k (i.e. column). We see that we can make this step by simply adding
        //              2 to our 6k-1 value. You can see this by solving for k in one equation and plugging it in for
        //              the other. After simplifying you will see the result is "2".
        //          So we now see that to go from a -1 row to a +1 row we add 2 to our value.
        //      Now observe that to go from a +1 row value to the -1 row value (to hit the next smallest value),
        //          we must take a step that is whatever the step from the -1 row to the +1 row is, subtracted from 6.
        //          That is, we want to take a step the size of whatever would be left over given our +2 step from
        //              the -1 row to the +1 row.
        //          In other words since we add 6 to go from column to column for the same row, and we add 2 to go
        //              from -1 to +1 in the same column, to switch from +1 row to a -1 row we need a step that covers
        //              the difference, so that ultimately when we end up in the next column to the right of the +1 row
        //              we have added 6 to the previous +1 row value.
        //          And thus when in row +1, we need a step of size 4 from our previous value to get to the next legal 
        //              value in the -1 row 
        //      To summarize our findings:
        //          To go from the -1 row to the +1 row we simply add 2.
        //          To go from the +1 row to the -1 row we need to add 4.
        //          This all leads to our next currentStep is simply 6 - our current step.
        //              So that 6 - 4 = 2 and 6 = 2 = 4.
        currentStep = 6 - currentStep;
    }

    // We have reached the end without breaking out, so the number must be prime
    return 1;
}

// TODO: Right now numPrimeFactors needs to be initialized to 0, this is the responsibility of the caller...
void DoPrimeFactorization( int val, int** out, int* numPrimeFactors )
{
    if( val < 0 )
    {
        printf("Value must be a positive integer! Val received got: %i\n", val);
        return;
    }

    if(val == 1)
    {
        printf("1 is called 'unit' and has no prime factors. It is neither prime or composite.\n");
        return;
    }

    ValidateObject( *out, "primeFactorizationOut" );

    // Initialize to 0 for conditional testing purposes
    int leftPairValue = 0;
    int rightPairValue = 0;
    
    // Start of assuming the value is prime. This covers the case val = 2 (we would of otherwise skipped the loop)
    leftPairValue = val;
    rightPairValue = 1;

    // Cycle through values starting at 2 and seek a factor pair to analyze
    for(int i = 2; i < val; i++ )
    {
        // Did we find a factor pair?
        if( IsFactor(val, i) )
        {
            leftPairValue = i;
            rightPairValue = val / i;
            //printf("leftPairValue: %i\n", leftPairValue);
            //printf("rightPairValue: %i\n", rightPairValue);
            break; // We just care about the first pair that we come across
        }
    }

    // Is the left value in the factor pair prime?
    if( !IsPrime(leftPairValue) )
    {
        DoPrimeFactorization( leftPairValue, out, numPrimeFactors );
    }
    // It is prime, add it to our list of prime factors
    else
    {
        *out = realloc( *out, sizeof(int) * (*numPrimeFactors + 1) );
        (*out)[ (*numPrimeFactors)++ ] = leftPairValue;
    }

    // Is the right value in the factor pair prime?
    if( !IsPrime(rightPairValue) )
    {
        DoPrimeFactorization( rightPairValue, out, numPrimeFactors );
    }
    // It is prime, add it to our list of prime factors
    else
    {
        *out = realloc( *out, sizeof(int) * (*numPrimeFactors + 1) );
        (*out)[ (*numPrimeFactors)++ ] = rightPairValue;
    }

    return;
}

// Euclids algorithm to find the GCD -> A classic divide and conqure algorithm that aims to solve the simpler problem
// This algorithm takes advantage of the fact that if two numbers a and b share a common factor f...
// So that a = mf and b = nf, then a-b = mf-nf = f(m-n)
// Thus the result of subtracting the two numbers results in them still sharing that common factor.
// Also known as Greatest Common Factor
int FindGCD( int a, int b )
{
    

    // Since a negative number is negative even if a positive number is a factor (e.g. -6 = 3 * -2)
    //      the GREATEST factor of these two numbers would be positive. So we only care to work with positive numbers
    a = AbsoluteValue(a);
    b = AbsoluteValue(b);

    // If the two numbers are the same, then their largest common divisor is obviously that number
    if( a == b )
    {
        return a;
    }
    // If either are zero, then the other is the GCD, since all numbers divide 0 (0 divided by any number is 0)
    else if( a == 0 )
    {
        return b;
    }
    else if( b == 0 )
    {
        return a;
    }

    // If a is larger than b, then we want to subtract b from a
    // Recursively call with this now reduced problem
    if( a > b )
    {
        return FindGCD(a-b, b);
    }
    // Otherwise subtract b from a, since a!=b (or else our above condition would of caught it) and we
    //      want to keep simplifying the problem 
    return FindGCD(a, b-a);
}


// TODO: COMMENT
// TODO: TEST
int FindLCM( int a, int b )
{
    int numPrimeFactorsOfA = 0;
    int* primeFactorsOfA = malloc(sizeof(int));
    ValidateObject(primeFactorsOfA, "primeFactorsOfA");
    DoPrimeFactorization( a, &primeFactorsOfA, &numPrimeFactorsOfA );

    int numPrimeFactorsOfB = 0;
    int* primeFactorsOfB = malloc(sizeof(int));
    ValidateObject(primeFactorsOfB, "primeFactorsOfB");
    DoPrimeFactorization( b, &primeFactorsOfB, &numPrimeFactorsOfB );

    int numPrimeFactorsOfResult = 0;
    int* primeFactorsOfResult = malloc(sizeof(int));
    ValidateObject(primeFactorsOfResult, "primeFactorsOfResult");
    
    for( int i = 0; i < numPrimeFactorsOfA; i++ )
    {
        for( int j = 0; j < numPrimeFactorsOfB; j )
        {
            // TODO: This may not handle case when the end of the B list contains our desired value as well
            // Objective: Get rid of the item from B list and resize B list
            if( primeFactorsOfA[i] == primeFactorsOfB[j] )
            {
                primeFactorsOfResult[numPrimeFactorsOfResult++] = primeFactorsOfA[i];
                primeFactorsOfResult = realloc( primeFactorsOfResult, sizeof(int) * (numPrimeFactorsOfResult + 1) );

                primeFactorsOfB[j] = primeFactorsOfB[numPrimeFactorsOfB - 1];
                primeFactorsOfB = realloc( primeFactorsOfB, sizeof(int) * (--numPrimeFactorsOfB) );
                ValidateObject(primeFactorsOfB, "primeFactorsOfB");
            }
            else // Don't want to move j up since we have put a new value to consider in the j spot
                j++;
        }
    }

    int answer = 1;
    for( int i = 0; i < numPrimeFactorsOfResult; i++ )
    {
        answer *= primeFactorsOfResult[i];
    }


    free(primeFactorsOfA);
    free(primeFactorsOfB);
    free(primeFactorsOfResult);

    return answer;
}





