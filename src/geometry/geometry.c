
#include <stdio.h>
#include <stdlib.h>
#include <common.h>
#include <my_math.h>
#include <geometry.h>


Line* CreateLinePoints(Point2D p1, Point2D p2, LineType lineType)
{
    Line* line = malloc(sizeof(Line));
    ValidateObject(line, "line");

    line->p1 = p1;
    line->p2 = p2;

    line->lineType = lineType;

    float sideA = AbsoluteValue( line->p1.x - line->p2.x );
    float sideB = AbsoluteValue( line->p1.y - line->p2.y );

    line->length = DoPythagoreanTheorem( sideA, sideB );
    
    //line->slope = dd
}


float DoPythagoreanTheorem(float a, float b)
{
    return SquareRoot( (a * a) + (b * b) );
}












