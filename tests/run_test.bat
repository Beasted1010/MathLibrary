
@echo off

:: Take parameter passed into this batch file and run that test

if [%1]==[] (
    echo Must have test directory to execute from as parameter!
    echo Exiting...
    exit /B
)

if "%1"=="h" (
    echo USAGE: run_test.bat <test_folder_name^>
    echo NOTE: If <test_folder_name^> is my_math execution will be in current directory (out of tests/bin folder^)
    exit /B
)

:: Execute executable in respective directory
if "%1"=="my_math" (
    %cd%/bin/%1_tests.exe
) else (
    %cd%/%1/bin/%1_tests.exe
)


