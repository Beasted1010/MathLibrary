
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <my_math.h>

#define DEFAULT_STOP_VAL 10000


void TestPrimeFactorization(int start_val, int stop_val, int step_divisor);
void TestIsPrime(int val);
void TestIsFactor(int val, int factor_to_test);
void TestFindGCD(int start_val, int stop_val);
void TestSquareRoot(float val);
void TestNthRoot(int nth_root, float val);


int main(int argc, char** argv)
{
    srand(time(0));
    
    //TestPrimeFactorization(1, DEFAULT_STOP_VAL, DEFAULT_STOP_VAL / 10);
    //TestFindGCD(20, 90);
    TestSquareRoot(1231);
    TestNthRoot(4, 4.5);

    return 0;
}

void TestFindGCD(int start_val, int stop_val)
{
    // Choose some random step to increment by
    int step;
    for( int i = start_val; i <= stop_val; i += step )
    {
        step = (rand() % stop_val) + 1;
        for( int j = 0; j <= stop_val; j += step )
        {
            printf("GCD of %i, %i, is: %i\n", i, j, FindGCD( i, j ));
        }
    }
}

void TestPrimeFactorization(int start_val, int stop_val, int step_divisor)
{
    int step;

    // Our step each iteration is random.
    for(int val = start_val; val <= stop_val; val += step)
    {
        int* result = malloc(sizeof(int));
        int numPrimeFactors = 0;

        printf("VAL: %i\n", val);
        DoPrimeFactorization(val, &result, &numPrimeFactors);

        if(!numPrimeFactors)
        {
            printf("No prime factors for %i\n", val);
            step = ((rand() % (stop_val / step_divisor)) + 1);
            free(result);
            continue;
        }

        for(int i = 0; i < numPrimeFactors; i++)
        {
            printf("PrimeFactor %i: %i\n", i, result[i]);
        }
        printf("\n");
    
        free(result);

        step = ((rand() % ((stop_val / step_divisor) + 1)) + 1);
        //printf("STEP: %i\n", step);
    }
}

void TestIsPrime(int val)
{
    printf("Is %i prime? (1=yes): %i\n", val, IsPrime(val));
}

void TestIsFactor(int val, int factor_to_test)
{
    printf("Is %i a factor of %i? (1=yes): %i\n", factor_to_test, val, IsFactor(val, factor_to_test));
}

void TestSquareRoot(float val)
{
    printf("Square root of %f = %f\n", val, SquareRoot(val));
}

void TestNthRoot(int nth_root, float val)
{
    printf("%i root of %f = %f\n", nth_root, val, NthRoot(nth_root, val));
}


