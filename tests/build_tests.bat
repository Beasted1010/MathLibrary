
@echo off

cd algebra/bin
        
gcc -o algebra_test.exe^
        -I ../../../inc^
        -I ../../../inc/algebra^
        -I ../../../inc/linear_algebra^
        -I ../../../inc/geometry^
        -I ../../../inc/trigonometry^
        -I ../../../inc/calculus^
        -I ../../../inc/computer_science^
        -L ../../../lib^
        ../algebra_tests.c

cd ../../linear_algebra/bin
        
gcc -o linear_algebra_test.exe^
        -I ../../../inc^
        -I ../../../inc/algebra^
        -I ../../../inc/linear_algebra^
        -I ../../../inc/geometry^
        -I ../../../inc/trigonometry^
        -I ../../../inc/calculus^
        -I ../../../inc/computer_science^
        -L ../../../lib^
        ../linear_algebra_tests.c

cd ../../geometry/bin
        
gcc -o geometry_test.exe^
        -I ../../../inc^
        -I ../../../inc/algebra^
        -I ../../../inc/linear_algebra^
        -I ../../../inc/geometry^
        -I ../../../inc/trigonometry^
        -I ../../../inc/calculus^
        -I ../../../inc/computer_science^
        -L ../../../lib^
        ../geometry_tests.c
    
cd ../../trigonometry/bin
    
gcc -o trigonometry_test.exe^
        -I ../../../inc^
        -I ../../../inc/algebra^
        -I ../../../inc/linear_algebra^
        -I ../../../inc/geometry^
        -I ../../../inc/trigonometry^
        -I ../../../inc/calculus^
        -I ../../../inc/computer_science^
        -L ../../../lib^
        ../trigonometry_tests.c
        
cd ../../calculus/bin
        
gcc -o calculus_test.exe^
        -I ../../../inc^
        -I ../../../inc/algebra^
        -I ../../../inc/linear_algebra^
        -I ../../../inc/geometry^
        -I ../../../inc/trigonometry^
        -I ../../../inc/calculus^
        -I ../../../inc/computer_science^
        -L ../../../lib^
        ../calculus_tests.c
        
cd ../../computer_science/bin
        
gcc -o computer_science_tests.exe^
        -I ../../../inc^
        -I ../../../inc/algebra^
        -I ../../../inc/linear_algebra^
        -I ../../../inc/geometry^
        -I ../../../inc/trigonometry^
        -I ../../../inc/calculus^
        -I ../../../inc/computer_science^
        -L ../../../lib^
        ../computer_science_tests.c

cd ../../bin

gcc -o my_math_tests.exe^
        -I ../../inc^
        -I ../../inc/algebra^
        -I ../../inc/linear_algebra^
        -I ../../inc/geometry^
        -I ../../inc/trigonometry^
        -I ../../inc/calculus^
        -I ../../inc/computer_science^
        -L ../../lib^
        ../my_math_tests.c^
        -lmath

cd ../



