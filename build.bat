
@echo off

cd obj

gcc -c^
        -I ../inc^
        -I ../inc/algebra^
        -I ../inc/linear_algebra^
        -I ../inc/geometry^
        -I ../inc/trigonometry^
        -I ../inc/calculus^
        -I ../inc/computer_science^
        ../src/my_math.c^
        ../src/algebra/algebra.c^
        ../src/linear_algebra/linear_algebra.c^
        ../src/geometry/geometry.c^
        ../src/trigonometry/trigonometry.c^
        ../src/calculus/calculus.c^
        ../src/computer_science/computer_science.c

cd ../lib

ar rcs libmath.a^
        ../obj/my_math.o^
        ../obj/algebra.o^
        ../obj/linear_algebra.o^
        ../obj/geometry.o^
        ../obj/trigonometry.o^
        ../obj/calculus.o^
        ../obj/computer_science.o

cd ..
